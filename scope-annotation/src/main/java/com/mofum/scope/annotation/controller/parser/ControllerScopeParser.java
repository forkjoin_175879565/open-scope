package com.mofum.scope.annotation.controller.parser;

import com.mofum.scope.annotation.controller.ControllerScope;
import com.mofum.scope.annotation.controller.ServiceColumn;
import com.mofum.scope.common.annotation.AnnotationParser;
import com.mofum.scope.common.annotation.metadata.controller.MControllerScope;
import com.mofum.scope.common.annotation.metadata.controller.MServiceColumn;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 控制器Scope解析器
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-21
 **/
public class ControllerScopeParser<T> implements AnnotationParser<Class<T>, MControllerScope> {
    public Map<Field, MControllerScope> parseField(Class<T> tClass) {

        Map<Field, MControllerScope> map = new HashMap<Field, MControllerScope>();

        Field[] fields = tClass.getDeclaredFields();

        for (Field field : fields) {

            if (field.isAnnotationPresent(ControllerScope.class)) {

                ControllerScope atColumn = field.getAnnotation(ControllerScope.class);

                map.put(field, parse(atColumn));

            }

        }

        return map;
    }

    public Map<Type, MControllerScope> parseType(Class<T> tClass) {

        Map<Type, MControllerScope> map = new HashMap<Type, MControllerScope>();

        Class<?> clazz = tClass;

        if (clazz.isAnnotationPresent(ControllerScope.class)) {

            ControllerScope atColumn = clazz.getAnnotation(ControllerScope.class);

            map.put(clazz, parse(atColumn));

        }

        return map;
    }

    public Map<Method, MControllerScope> parseMethod(Class<T> tClass) {

        Map<Method, MControllerScope> map = new HashMap<Method, MControllerScope>();

        Method[] methods = tClass.getDeclaredMethods();

        for (Method method : methods) {

            if (method.isAnnotationPresent(ControllerScope.class)) {

                ControllerScope atColumn = method.getAnnotation(ControllerScope.class);

                map.put(method, parse(atColumn));

            }

        }

        return map;
    }

    public Map<Method, List<MControllerScope>> parseParams(Class<T> tClass) {

        Map<Method, List<MControllerScope>> map = new HashMap<Method, List<MControllerScope>>();

        Method[] methods = tClass.getDeclaredMethods();

        for (Method method : methods) {

            List<MControllerScope> list = new ArrayList<MControllerScope>();

            Annotation[][] parameterAnnotations = method.getParameterAnnotations();

            for (Annotation[] parameterAnnotation : parameterAnnotations) {
                for (Annotation annotation : parameterAnnotation) {
                    if (annotation instanceof ControllerScope) {
                        ControllerScope atColumn = (ControllerScope) annotation;
                        list.add(parse(atColumn));
                    }
                }
            }

            map.put(method, list);
        }

        return map;
    }

    public MControllerScope parse(ControllerScope controllerScope) {
        MControllerScope mControllerScope = new MControllerScope();
        mControllerScope.setEnableAuthenticator(controllerScope.enableAuthenticator());
        mControllerScope.setExtractors(controllerScope.extractors());
        mControllerScope.setAuthenticators(controllerScope.authenticators());
        mControllerScope.setEnableExtractor(controllerScope.enableExtractor());
        mControllerScope.setUpdateMethods(controllerScope.updateMethods());
        mControllerScope.setIgnoreMethods(controllerScope.ignoreMethods());
        mControllerScope.setQueryMethods(controllerScope.queryMethods());
        mControllerScope.setEnableConverter(controllerScope.enableConverter());
        mControllerScope.setConverters(controllerScope.converts());
        mControllerScope.setServiceColumns(controllerScope.serviceColumns());
        mControllerScope.setEnableCustomServiceColumns(controllerScope.enableCustomServiceColumns());
        mControllerScope.setType(controllerScope.type());

        ServiceColumn[] serviceColumns = controllerScope.columns();
        String[] stringServiceColumns = controllerScope.serviceColumns();
        String type = controllerScope.type();

        Map<String, MServiceColumn> serviceColumnMap = new HashMap<>();

        if (stringServiceColumns != null) {
            for (int i = 0; i < stringServiceColumns.length; i++) {
                MServiceColumn serviceColumn = new MServiceColumn();
                serviceColumn.setName(stringServiceColumns[i]);
                serviceColumn.setType(type);
                serviceColumnMap.put(stringServiceColumns[i], serviceColumn);
            }
        }

        if (serviceColumns != null) {

            ServiceColumnParser serviceColumnParser = new ServiceColumnParser();
            for (int i = 0; i < serviceColumns.length; i++) {
                MServiceColumn mServiceColumn = serviceColumnParser.parse(serviceColumns[i]);
                serviceColumnMap.put(mServiceColumn.getName(), mServiceColumn);
            }
        }
        MServiceColumn[] res = new MServiceColumn[serviceColumnMap.size()];

        int i = 0;
        for (String key : serviceColumnMap.keySet()) {
            res[i] = serviceColumnMap.get(key);
            i++;
        }
        mControllerScope.setColumns(res);

        return mControllerScope;
    }
}
