package com.mofum.scope.annotation.controller;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ServiceColumn {

    /**
     * 列名
     *
     * @return
     */
    String value() default "";

    /**
     * 类型
     *
     * @return
     */
    String type() default "";

    /**
     * 是否是数组
     *
     * @return
     */
    boolean array() default false;

    /**
     * 分割正则表达式
     *
     * @return
     */
    String splitRegex() default ",";

    /**
     * 方法名
     *
     * @return
     */
    String method() default "";

    /**
     * JSON 数据
     * @return
     */
    boolean json() default false;
}
