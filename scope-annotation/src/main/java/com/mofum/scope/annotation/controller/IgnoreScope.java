package com.mofum.scope.annotation.controller;


import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IgnoreScope {

    String value() default "";

    /**
     * 更新方法
     *
     * @return
     */
    String[] updateMethods() default {};

    /**
     * 查询方法
     *
     * @return
     */
    String[] queryMethods() default {};

    /**
     * 更新反转
     *
     * @return
     */
    boolean updateReversal() default false;

    /**
     * 查询反转
     *
     * @return
     */
    boolean queryReversal() default false;

}
