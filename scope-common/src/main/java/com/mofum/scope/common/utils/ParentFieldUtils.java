package com.mofum.scope.common.utils;

import java.lang.reflect.Field;

/**
 * @author yuyang@qxy37.com
 * @since 2019-03-22
 **/
public class ParentFieldUtils {


    public static Field getFieldByName(String name, Class<?> tClass){

        Field field = null;
        try {
            field = tClass.getDeclaredField(name);
        } catch (NoSuchFieldException e) {
            field = null;
        }

        Class<?> supperClass = tClass.getSuperclass();

        if (field == null && !supperClass.equals(Object.class)) {
            return getFieldByName(name, supperClass);
        }
        return field;

    }

}
