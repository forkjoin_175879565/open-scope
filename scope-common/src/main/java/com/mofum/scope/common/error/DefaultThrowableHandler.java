package com.mofum.scope.common.error;

/**
 * 默认的异常的处理器
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-27
 **/
public class DefaultThrowableHandler implements ThrowableHandler {

    @Override
    public void handler(Throwable e) throws Throwable {
        if (e != null) {
            throw e;
        }

    }
}
