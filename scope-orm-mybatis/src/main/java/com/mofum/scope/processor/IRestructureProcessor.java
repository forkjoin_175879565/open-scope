package com.mofum.scope.processor;

import com.mofum.scope.common.compatible.IScopeCompatibleListener;
import com.mofum.scope.common.model.Permission;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;

/**
 * @author yumi@omuao.com
 * @since 2019-03-19
 **/
public interface IRestructureProcessor {

    /**
     * 处理SQL
     *
     * @param ms            调用
     * @param permissionSql 权限SQL
     * @param permission    权限实体
     * @return
     */
    BoundSql process(MappedStatement ms, BoundSql permissionSql, Permission permission);

    /**
     * 设置兼容监听器
     *
     * @param listener 监听器
     */
    void setCompatibleListener(IScopeCompatibleListener listener);
}